# Problem Statement
```
Create a chess board where queen can only be installed on periphery positions and runs through each block to return to its periphery position.
Note: Queen can only run through peripheral blocks to return to its original position
```

## Steps to run
```
Just run index.html in your browser
```
```
Copy past the address of index.html file in your browser URL to run
```

## Demo
```
https://codepen.io/karanraijyada/pen/vYEMVjm
```