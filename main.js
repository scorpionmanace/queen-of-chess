'use strict';

// Global Chess Object
let chess = {};

// Render Chess Board
chess.rows = 8;
chess.columns = 8;
chess.renderBoard = function() {
    const that = this;
    const chessBoard = document.getElementById('chess');
    const rows = that.rows;
    const cols = that.columns;
    for (let row = rows; row >= 1; row--) {
        for (let col = cols; col >= 1; col--) {
            const square = `<div class='square' row='${row}' col='${col}' value='${row+''+col}'></div>`;
            chessBoard.innerHTML += square;
        }
    }
};

// Chess directions for movements
chess.directions = {
    //   primary directions
    'left': {
        val: 'left',
        row: 0,
        col: -1
    },
    'right': {
        val: 'right',
        row: 0,
        col: 1
    },
    'top': {
        val: 'top',
        row: 1,
        col: 0
    },
    'bottom': {
        val: 'bottom',
        row: -1,
        col: 0
    }
    // Other auxilary directions top-left, top-right, bottom-left, bottom-right (not needed for this problem)
    // Not implemented,as not requrired for this application
}

// map of actor(s) and their respective doms
chess.actors = {
    'queen': {
        val: 'queen',
        dom: `<div class="actor queen"><img src="https://images.vexels.com/media/users/3/143216/isolated/preview/2ae1ced518237938f0aa487655a0362b-queen-chess-figure-black-by-vexels.png"/></div>`
    }
};

// Redering actor on chess board
chess.render = function(actor, row, col) {
        const that = this;
        if (!that.actors[actor]) {
            throw 'Incorrect Actor';
        }
        const currentActor = that.actors[actor];
        const currentBlock = document.querySelectorAll('div[value="' + row + '' + col + '"]');
        currentBlock[0].innerHTML = currentActor.dom;
    }
    
// Clear the actor from the square block
chess.clearRender = function(actor, row, col) {
    const that = this;
    if (!that.actors[actor]) {
        throw 'Incorrect Actor';
    }
    const currentBlock = document.querySelectorAll('div[value="' + row + '' + col + '"]');
    currentBlock[0].innerHTML = '';
}

// Rendering movement as per the problem statement
chess.moveActor = function(actor, row, col, endRow, endColumn, direction) {
    const that = this;
    let currentRow = row;
    let currentCol = col;
    let prevRow = row;
    let prevCol = col;
    button.disable();
    const interval = setInterval(() => {
        prevRow = currentRow;
        prevCol = currentCol;
        //       move right ot bottom/reverse
        if (currentRow === that.rows && currentCol === 1) {
            direction = direction === that.directions.top.val ? that.directions.right.val : that.directions.bottom.val
        }
        //       move left or bottom/reverse
        if (currentRow === that.rows && currentCol === that.columns) {
            direction = direction === that.directions.top.val ? that.directions.left.val : that.directions.bottom.val
        }
        //       move right or top
        if (currentRow === 1 && currentCol === that.columns) {
            direction = direction === that.directions.bottom.val ? that.directions.left.val : that.directions.top.val
        }
        //       move left or top
        if (currentRow === 1 && currentCol === 1) {
            direction = direction === that.directions.bottom.val ? that.directions.right.val : that.directions.top.val
        }
        currentRow += that.directions[direction].row;
        currentCol += that.directions[direction].col;
        that.render(actor, currentRow, currentCol);
        that.clearRender(actor, prevRow, prevCol);
        if (currentRow === endRow && currentCol === endColumn) {
            button.enable();
            clearInterval(interval);
        }
    }, 1000)

}
// Reder the board
chess.renderBoard();

// Main execution method
const solution = (actor, row, col, direction = chess.directions.right.val) => {
    chess.render(actor, row, col, direction);
    chess.moveActor(actor, row, col, row, col, direction)
}
// Helper methods to verify valid row and column is entered in the input fields
const validate = {
    row: (row) => {
        return (row === 1 || row===chess.rows);
            
    },
    column: (col) => {
        return (col >= 1 && col <= chess.columns);
    }
}
// enabling or disabling the button
const button = {
    enable: () => {
        const btn = document.getElementById('playChess');
        btn.classList.remove('disabled');
        btn.disabled = false;
    },
    disable: () => {
        const btn = document.getElementById('playChess');
        btn.classList.add('disabled');
        btn.disabled = true;
    }
}

const messages = {};
// Generate error messages
messages.generateErrorMessages = (invalidRow = false, invalidColumn = false) => {
    let msg ='';
    const messageContainer = document.getElementsByClassName('message-container');
    const messages = document.getElementById('messages');
    if (invalidRow && invalidColumn) {
        msg = 'Invalid Row (1 or 8) and Column (1 through 8) Entered';
    } else if (invalidRow) {
        msg = 'Invalid Row value enter, accepted 1 or 8';
    } else {
        msg = 'Invalid Column value entered, accpeted 1 through 8';
    }

    messages.innerHTML =`<p>Error: ${msg}</p>`;
    messageContainer[0].classList.remove('d-none');

}
// Clear error messages
messages.clearMessages = () => {
    const messageContainer = document.getElementsByClassName('message-container');
    messageContainer[0].classList.add('d-none');
}
const globals = {
    prevRow: 1,
    prevCol: 1
};
document.getElementById('playChess').addEventListener('click', () => {
    // Variables for row, column and actor
    const currentRow = parseInt(document.getElementById('startRow').value,10);
    const currentColumn = parseInt(document.getElementById('startCol').value);
    const currentActor = 'queen';
    messages.clearMessages();
    chess.clearRender(currentActor,globals.prevRow,globals.prevCol);
    if (validate.row(currentRow) && validate.column(currentColumn)){
        // Execution    
        solution(currentActor, currentRow, currentColumn);
        globals.prevRow = currentRow;
        globals.prevCol = currentColumn;
    } else {
        messages.generateErrorMessages(!validate.row(currentRow), !validate.column(currentColumn));
    }
});
